import { Ex8Page } from './app.po';

describe('ex8 App', () => {
  let page: Ex8Page;

  beforeEach(() => {
    page = new Ex8Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
