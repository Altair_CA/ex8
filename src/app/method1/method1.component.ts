
import { Component, OnInit } from '@angular/core';

// use when there is typings
import * as jQuery from 'jquery';
import * as _ from 'underscore';

//use when their is no typings
/*
declare var jquery:any;
declare var $ :any;
declare var _:any;
*/

@Component({
  selector: 'app-method1',
  templateUrl: './method1.component.html',
  styleUrls: ['./method1.component.css']
})

export class Method1Component implements OnInit {

  constructor() {

    var debounced = _.debounce(function (text) {
      var url = "https://api.spotify.com/v1/search?type=artist&q=" + text;
      $.getJSON(url, function (artists) {
        console.log(artists);
      });
    }, 400);
    $(document).ready(function () {
      $("#search").keyup(function (e) {
      
        var text =<String> $(this).val();
        console.log(text);
        if (text.length < 3) {
          return;
        }

        debounced(text);
        

      });
    });
    


  }

  ngOnInit() {
  }

}
