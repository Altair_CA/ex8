import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControl,FormBuilder} from '@angular/forms';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.css']
})
export class PdfComponent implements OnInit {
  form:FormGroup;

  constructor(fb: FormBuilder) {
    //subscribe to input field using angular way
    this.form = fb.group({
        search : [""]
    });
    var search = this.form.get('search');
    console.log(search);
    search.valueChanges
    .debounceTime(400)
    .map(str => (<string>str).replace(' ', '-'))
    .subscribe(
      x => console.log(x)
    );

//using array
    var startDates = [];
    var starDate = new Date();

    for(var day = -2; day <= 2; day++){
      var date = new Date(
        starDate.getFullYear(),
        starDate.getMonth(),
        starDate.getDate() + day);
      startDates.push(date);
    }

    Observable.from(startDates)
      .map(date => {
        console.log("Getting deals for date " + date);
        return [date.getFullYear(),date.getMonth(),date.getDate()];
      })
      .subscribe(x=> console.log(x));

// using interval
    var intervalObservable = Observable.interval(1000);
    intervalObservable
      .map(x => {
        console.log("calling server to get the latest news");
        return [1,2,3];
      })
      .subscribe(x => console.log(x));

// running obserables in parallel
    var userStream = Observable.of({
      userId: 1, username: 'gg'
    }).delay(2000);

    var tweetsStream = Observable.of([1,2,3]).delay(1500);

    Observable.forkJoin(userStream,tweetsStream)
      .map(x=> new Object({user: x[0], tweets: x[1]}))
      .subscribe(x=> console.log(x));

      //Error handling
      var counter = 0;
      var ajaxCall = Observable.of('url')
                        .flatMap(() => {
                            if(++counter < 2)
                              return Observable.throw(new Error("Request failed"));
                            
                            return Observable.of([6,5,9]);
                        })
                        .retry(3)
                        .subscribe(
                          x => console.log(x),
                          error => console.log(error)
                        );
  }

  
  ngOnInit() {
  }

}
