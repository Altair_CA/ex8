import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import * as jQuery from 'jquery';
import * as _ from 'underscore';

@Component({
  selector: 'app-method2',
  templateUrl: './method2.component.html',
  styleUrls: ['./method2.component.css']
})
export class Method2Component implements OnInit {

  constructor() {
    $(document).ready(function () {
      var $input = $('#search')[0];
      //console.log($input);
      var keyups = Observable.fromEvent<KeyboardEvent>($input,"keyup" )
      .map(e=> (<HTMLInputElement>e.target).value)
      .filter(text => text.length >= 3)
      .debounceTime(400)
      .distinct()
      .flatMap(searchTerm => {
          var url="https://jsonplaceholder.typicode.com"+searchTerm;
          var promise = $.getJSON(url);
          return Observable.fromPromise(promise);
      });
      
      //console.log(keyups);

      var subscriptions = keyups.subscribe(data=> console.log(data));
      
    });
  }

  ngOnInit() {
  }

}
